use crate::github::Payload;
use actix_web::{web, App, HttpServer, HttpRequest, HttpResponse};
use actix_web::web::Bytes;

use crypto::hmac::Hmac;
use crypto::mac::Mac;
use crypto::sha1::Sha1;

use itertools::Itertools;
use std::env;

pub mod github {
    use serde::Deserialize;
    use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT, CONTENT_TYPE, ACCEPT};
    use reqwest;
    use std::collections::HashMap;
    use std::env;

    #[derive(Deserialize)]
    pub struct Comment {
        pub id: i64,
        pub body: String,
    }

    #[derive(Deserialize)]
    pub struct Repository {
        pub full_name: String,
    }

    #[derive(Deserialize)]
    pub struct Payload {
        pub repository: Repository,
        pub comment: Comment,
    }

    fn construct_headers() -> HeaderMap {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_static("reqwest"));
        headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
        headers.insert(ACCEPT, HeaderValue::from_static("application/vnd.github.v3+json"));
        headers
    }

    fn construct_auth() -> (Option<String>, Option<String>) {
        let mut username = String::new();
        let mut token = String::new();
        for (key, value) in env::vars() {
            if key.eq("GITHUB_COMMENTER_USERNAME") {
                username = value.clone();
            }
            if key.eq("GITHUB_COMMENTER_TOKEN") {
                token = value.clone();
            }
        }

        (Some(username), Some(token))
    }

    /// changes content of a comment
    pub fn edit_comment(full_name: &str, id: i64, new_body: &str)  {
        println!("Preparing to send payload with new body");

        let mut map = HashMap::new();
        map.insert("body", new_body);

        let (username, token) = construct_auth();
        let headers = construct_headers();

        let endpoint = format!("https://api.github.com/repos/{}/issues/comments/{}", full_name, id);
        let client = reqwest::blocking::Client::new();

        let resp = client.patch(&endpoint)
            .json(&map)
            .headers(headers)
            .basic_auth(username.unwrap(), token)
            .send();

        println!("{:?}", resp);

        match resp {
            Ok(v) => println!("got success from server: {:?}", v),
            Err(e) => println!("got error from server: {:?}", e),
        }
    }

    pub fn markdown_gif(url: &str, alt: &str) -> String {
        format!("![{}]({})", alt, url)
    }
}

pub mod jennifer {
    use crate::github::markdown_gif;

    pub enum Mood {
        Good,
        Bad,
        Ugly,
    }

    pub fn get(mood: Mood) -> &'static str {
        match mood {
            Mood::Good => "https://media.giphy.com/media/a3zqvrH40Cdhu/giphy.gif",
            Mood::Bad => "https://media.giphy.com/media/TaIUz912KQVi/giphy.gif",
            Mood::Ugly => "https://media.giphy.com/media/cszUrZAPfUTMQ/giphy.gif",
        }
    }

    pub fn edit_comment(comment: &str) -> String {
        comment.to_string()
            .replace("/jenniferlawrence good", &markdown_gif(get(Mood::Good), "good"))
            .replace("/jenniferlawrence bad", &markdown_gif(get(Mood::Bad), "bad"))
            .replace("/jenniferlawrence ugly", &markdown_gif(get(Mood::Ugly), "ugly"))
            .replace("/jenniferlawrence", &markdown_gif(get(Mood::Good), "good"))
    }
}

fn compute_hmac_content(message: &[u8]) -> String {
    let secret = match env::var("GITHUB_SECRET") {
        Ok(secret) => secret,
        Err(_) => return String::from("")
    };

    let mut mac = Hmac::new(Sha1::new(), secret.as_bytes());
    mac.input(&message);
    mac.result().code().iter().format_with("", |byte, f| f(&format_args!("{:02x}", byte))).to_string()
}

fn payload(req: HttpRequest, body: Bytes) -> HttpResponse {
    let signature = match req.headers().get("X-Hub-Signature") {
        Some(signature) => signature,
        None => return HttpResponse::NotFound().into(),
    };

    let signature = match signature.to_str() {
        Ok(signature) => &signature[5..], // discard 'sha1='
        Err(_) => return HttpResponse::NotFound().into(),
    };

    let code = compute_hmac_content(&body);
    if signature != code {
        println!("Signature does not correspond");
        return HttpResponse::NotFound().into()
    }

    let payload: Payload = match serde_json::from_slice(&body) {
        Ok(payload) => payload,
        Err(_) => return HttpResponse::BadRequest().into() // nothing happens
    };

    if payload.comment.body.contains("/jenniferlawrence") {
        let jennifered_comment = jennifer::edit_comment(&payload.comment.body);
        github::edit_comment(&payload.repository.full_name, payload.comment.id, &jennifered_comment);
    } else {
        return HttpResponse::NotModified().into()
    }

    HttpResponse::Ok().into()
}


fn main() {
    println!("Starting server");
    HttpServer::new(|| {
        App::new()
            .route("/payload", web::post().to(payload))
    })
        .bind("127.0.0.1:8088")
        .unwrap()
        .run()
        .unwrap();
}
