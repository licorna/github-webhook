FROM rust:1.40-stretch as builder

WORKDIR /usr/src/github-webhook
COPY . .

RUN cargo install --path .

FROM alpine

COPY --from=builder /usr/local/cargo/bin/github-webhook /usr/bin/github-webhook
ENTRYPOINT ["/usr/bin/github-webhook"]
